console.log("Hello, I'm Elysha");


// username function
let username = prompt("Username: ");

function loginDetailsUsername(){

  if (username === "" ){
   
      alert("input should not be empty");

  } else if (username === null){
    alert("input should not be null");
  }
}

loginDetailsUsername();

// end of usernament

// start of password function

let password = prompt("Password: ");


function loginDetailsPassword(){

  if (password === "" ){
   
      alert("input should not be empty");

  } else if (password === null){
    alert("input should not be null");
  }
}

loginDetailsPassword();

// end of password function

// start of role function
let role     = prompt("Role: ");

function loginDetailsRole(){

  if (role === "" ){
   
      alert("input should not be empty");

  } else if (role === null){
    alert("input should not be null");
  } else {
    switch(role){
      case 'admin':
        alert("Welcome back to the class portal, admin!");
        break;
      
      case 'teacher':
        alert("Thank you for logging in, teacher!");
        break;
      
      case 'student':
        alert("Welcome to the class portal, student!");
        break;

      default:
        alert("Role out of range.");
    }
  }
}

loginDetailsRole();

// end of role function

// end of login


// start of average function

function receiveAverage(one,two,three,four){
  
  let average = Math.round((one+two+three+four)/4);
  console.log("Average: " + average);

    if (average <= 74){
      console.log("Hello, student, your average is " + average + " The letter equivalent is F");

    } else if (average >= 75 && average <= 79){
      console.log("Hello, student, your average is " + average + " The letter equivalent is D");

    }else if (average >= 80 && average <= 84){
      console.log("Hello, student, your average is " + average + " The letter equivalent is C");

    }else if (average >= 85 && average <=89){
      console.log("Hello, student, your average is " + average + " The letter equivalent is B");
    }else if (average >= 90 && average <=95){
      console.log("Hello, student, your average is " + average + " The letter equivalent is A");
    }else {
      console.log("Hello, student, your average is " + average + " The letter equivalent is A+");
    }

  return average;
}

receiveAverage(98.5,98,98,98);

